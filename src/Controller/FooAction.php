<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\BookRepository;

class FooAction
{
    public function __invoke(BookRepository $bookRepository)
    {
        /**
         * 12 id'ga ega bo'lgan  book'ni ber demoqdamiz.
         * Javob sifatida Book entity'si qaytadi.
         * Agar ma'lumot topilmasa - null qaytadi.
         */
        $book = $bookRepository->find(12);

        /**
         * Ma'lumotni shartlar boyicha qidirish mumkin.
         * SQL'da by: where `name` = 'Shaytanat' and `description` = 'Tohir Malikning katta romani'
         */
        $book = $bookRepository->findOneBy(
            [
                'name' => 'Tohir Malikning katta romani',
            ]
        );
    }
}
